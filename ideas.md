Calculating the next number is definitely not hard, one line can do it.
One interesting thing that we can find is that 89 will do some numbers until it reaches 89 again. We could create a list of these number and considers that when we reach one of them, we can add 1 to the 89 counter. 
We can then do the same for 1 and categorize each number below n to be either in 1s or 89s.

This will require to have 2 lists going quite big but, because they will fasten the process of research if a number is a 1 or a 89 it should still be relatively fast.

First, let's check the one liner list comprehension to get the sum of the square of the number:
print(sum(int(x)**2 for x in str(89))) --> It does give 145 so it works.