import time
start_time = time.time()

def find_square_digit_chain(upper_limit):
    eighty_nine_numbers = {89}
    one_numbers = {1}
    for i in range(2,upper_limit):
        next_num = i
        next_numbers = [i]
        found = False
        while found == False:
            if  next_num == 1:
                one_numbers.update(next_numbers)
                found = True
            elif next_num == 89:
                eighty_nine_numbers.update(next_numbers)
                found = True
            else:
                next_num = sum(int(x)**2 for x in str(next_num))
                next_numbers.append(next_num)
    print(max(eighty_nine_numbers))
    return len([x for x in eighty_nine_numbers if x < upper_limit])

print(find_square_digit_chain(10000000))
print(f"--- {(time.time() - start_time):.10f} seconds ---" )